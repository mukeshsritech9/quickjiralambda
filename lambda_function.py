import json
import http.client
import mimetypes

def lambda_handler(event, context):
        
    conn = http.client.HTTPSConnection("technine.atlassian.net")
    
    if 'basic_auth' not in event['param_headers']:
      event['param_headers']['basic_auth'] = 'ZGlwZW4ubmF3YW5pQHRlY2g5LmNvbTpPb0xWcDFQNXludmVjak9xN2E4WEY1QTQ'
      
    headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + event['param_headers']['basic_auth']
    }

    conn.request(event['method'], event['url'], json.dumps(event['params']), headers)
    res = conn.getresponse()
    data = res.read()
    
    
    return data
